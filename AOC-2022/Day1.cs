﻿namespace AOC_2022
{
    public class Day1
    {
        IEnumerable<Elf> elfs =new List<Elf>();

        public Elf MaxElf()
        {
            return this.OrderedElf().First();
        }
        public IEnumerable<Elf> OrderedElf()
        {
            return elfs.OrderBy(x => x.calories).Reverse();
        }

        public void Load(IEnumerable<string> data)
        {
            var collector=new List<Elf>();
            int i = 1;
            int cal = 0;
            foreach (var item in data)
            {
                if (string.IsNullOrEmpty(item))
                {
                    collector.Add(new Elf(i, cal));
                    i++;
                    cal = 0;
                }
                else
                {
                    cal += Int32.Parse(item);
                }
            }
            collector.Add(new Elf(i, cal));
            elfs = collector;
        }
    }
    public class Elf
    {
        public Elf(int i, int c)
        {
            id = i;
            calories = c;
        }
        public int id { get; set; }
        public int calories { get; set; }
    }
}