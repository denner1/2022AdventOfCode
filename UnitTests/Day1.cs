using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using AOC_2022;
using System.Diagnostics;
using System.Linq;

namespace UnitTests
{
    [TestClass]
    public class Day1Test
    {
        [TestMethod]
        public void Part1()
        {
            var input=File.ReadAllLines("Day1-1.txt");
            var SUT = new Day1();
            SUT.Load(input);
            var res=SUT.MaxElf();
            Trace.TraceInformation($"Max elf is {res.id} with {res.calories}");
        }
        [TestMethod]
        public void Part2()
        {
            var input = File.ReadAllLines("Day1-1.txt");
            var SUT = new Day1();
            SUT.Load(input);
            var res = SUT.OrderedElf().Take(3);
            var collect=res.Select(x => x.calories).Sum();
            Trace.TraceInformation($"we have {collect} cals");
        }
    }
}